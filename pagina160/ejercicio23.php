<?php
// Variables
$operador = '';
$nombreOperador = '';

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $operador = $_POST["txtOperador"];

    // Proceso
    switch ($operador) {
        case '+':
            $nombreOperador = 'Suma';
            break;
        case '-':
            $nombreOperador = 'Resta';
            break;
        case '*':
            $nombreOperador = 'Multiplicación';
            break;
        case '/':
            $nombreOperador = 'División';
            break;
        default:
            $nombreOperador = 'Operador inválido';
            break;
    }
}

?>

<html>
<head>
    <title>Problema 23</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio23.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 23</strong></td>
            </tr>
            <tr>
                <td>Operador aritmético</td>
                <td>
                    <input name="txtOperador" type="text" id="txtOperador" value="<?=$operador?>" />
                </td>
            </tr>
            <?php if ($nombreOperador != '') { ?>
            <tr>
                <td>Nombre del operador</td>
                <td>
                    <input name="txtNombreOperador" type="text" class="TextoFondo" id="txtNombreOperador" value="<?=$nombreOperador?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
