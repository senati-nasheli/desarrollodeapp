<?php
// Variables
$fecha = "";
$dias_faltantes = 0;

if(isset($_POST["btnCalcular"])) {
    $fecha = $_POST["txtFecha"];

    $fecha_actual = new DateTime();
    $fecha_ingresada = new DateTime($fecha);

    $ultimo_dia = new DateTime($fecha_actual->format('Y-12-31'));

    $diferencia = $ultimo_dia->diff($fecha_ingresada);
    $dias_faltantes = $diferencia->format('%a');
}

?>

<html>
<head>
    <title>Problema 30</title>
    <link rel="stylesheet" href="estilos30.css">
</head>
<body>
    <form method="post" action="ejercicio30.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 30</strong></td>
            </tr>
            <tr>
                <td>Fecha</td>
                <td>
                    <input name="txtFecha" type="date" value="<?=$fecha?>" />
                </td>
            </tr>
            <?php if ($dias_faltantes > 0) { ?>
            <tr>
                <td>Días faltantes</td>
                <td>
                    <input name="txtDiasFaltantes" type="text" class="TextoFondo" value="<?=$dias_faltantes?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
