<?php
// Variables
$numeroCanal = 0;
$nombreCanal = '';

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $numeroCanal = (int)$_POST["txtNumeroCanal"];

    // Proceso
    switch ($numeroCanal) {
        case 2:
            $nombreCanal = 'Canal de las Estrellas';
            break;
        case 5:
            $nombreCanal = 'Tv una';
            break;
        case 7:
            $nombreCanal = 'latina';
            break;
        case 9:
            $nombreCanal = 'america tv';
            break;
        case 11:
            $nombreCanal = 'Once TV';
            break;
        default:
            $nombreCanal = 'Canal desconocido';
            break;
    }
}

?>

<html>
<head>
    <title>Problema 24</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio24.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 24</strong></td>
            </tr>
            <tr>
                <td>Número de canal</td>
                <td>
                    <input name="txtNumeroCanal" type="text" id="txtNumeroCanal" value="<?=$numeroCanal?>" />
                </td>
            </tr>
            <?php if ($nombreCanal != '') { ?>
            <tr>
                <td>Nombre del canal</td>
                <td>
                    <input name="txtNombreCanal" type="text" class="TextoFondo" id="txtNombreCanal" value="<?=$nombreCanal?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
