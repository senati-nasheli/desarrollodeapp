<?php
// Variables
$codigo = 0;
$estadoCivil = "";

if(isset($_POST["btnObtener"])) {
    // Entrada
    $codigo = (int)$_POST["txtCodigo"];

    // Proceso
    switch ($codigo) {
        case 0:
            $estadoCivil = "Soltero";
            break;
        case 1:
            $estadoCivil = "Casado";
            break;
        case 2:
            $estadoCivil = "Divorciado";
            break;
        case 3:
            $estadoCivil = "Viudo";
            break;
        default:
            $estadoCivil = "Desconocido";
            break;
    }
}

?>

<html>
<head>
    <title>Problema 27</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio27.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 27</strong></td>
            </tr>
            <tr>
                <td>Código de estado civil</td>
                <td>
                    <input name="txtCodigo" type="text" value="<?=$codigo?>" />
                </td>
            </tr>
            <?php if (!empty($estadoCivil)) { ?>
            <tr>
                <td>Estado civil</td>
                <td>
                    <input name="txtEstadoCivil" type="text" class="TextoFondo" value="<?=$estadoCivil?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnObtener" type="submit" value="OBTENER" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
