<?php
// Variables
$numeroDia = 0;
$nombreDia = '';

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $numeroDia = (int)$_POST["txtNumeroDia"];

    // Proceso
    switch ($numeroDia) {
        case 1:
            $nombreDia = 'Domingo';
            break;
        case 2:
            $nombreDia = 'Lunes';
            break;
        case 3:
            $nombreDia = 'Martes';
            break;
        case 4:
            $nombreDia = 'Miércoles';
            break;
        case 5:
            $nombreDia = 'Jueves';
            break;
        case 6:
            $nombreDia = 'Viernes';
            break;
        case 7:
            $nombreDia = 'Sábado';
            break;
        default:
            $nombreDia = 'Día inválido';
            break;
    }
}

?>

<html>
<head>
    <title>Problema 22</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio22.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 22</strong></td>
            </tr>
            <tr>
                <td>Número de día</td>
                <td>
                    <input name="txtNumeroDia" type="text" id="txtNumeroDia" value="<?=$numeroDia?>" />
                </td>
            </tr>
            <?php if ($nombreDia != '') { ?>
            <tr>
                <td>Día de la semana</td>
                <td>
                    <input name="txtNombreDia" type="text" class="TextoFondo" id="txtNombreDia" value="<?=$nombreDia?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
