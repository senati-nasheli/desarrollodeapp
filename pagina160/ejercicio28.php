<?php
// Variables
$tiempoServicio = 0;
$cargo = "";
$montoUtilidades = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $tiempoServicio = (int)$_POST["txtTiempoServicio"];
    $cargo = $_POST["ddlCargo"];

    // Proceso
    if ($tiempoServicio >= 0 && $tiempoServicio <= 2) {
        switch ($cargo) {
            case "Administrador":
                $montoUtilidades = 2000;
                break;
            case "Contador":
                $montoUtilidades = 1500;
                break;
            case "Empleado":
                $montoUtilidades = 1000;
                break;
        }
    } elseif ($tiempoServicio >= 3 && $tiempoServicio <= 5) {
        switch ($cargo) {
            case "Administrador":
                $montoUtilidades = 2500;
                break;
            case "Contador":
                $montoUtilidades = 2000;
                break;
            case "Empleado":
                $montoUtilidades = 1500;
                break;
        }
    } elseif ($tiempoServicio >= 6 && $tiempoServicio <= 8) {
        switch ($cargo) {
            case "Administrador":
                $montoUtilidades = 3000;
                break;
            case "Contador":
                $montoUtilidades = 2500;
                break;
            case "Empleado":
                $montoUtilidades = 2000;
                break;
        }
    } elseif ($tiempoServicio > 8) {
        switch ($cargo) {
            case "Administrador":
                $montoUtilidades = 4000;
                break;
            case "Contador":
                $montoUtilidades = 3500;
                break;
            case "Empleado":
                $montoUtilidades = 1500;
                break;
        }
    }
}

?>

<html>
<head>
    <title>Problema 28</title>
    <link rel="stylesheet" href="estilos28.css">
</head>
<body>
    <form method="post" action="ejercicio28.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 28</strong></td>
            </tr>
            <tr>
                <td>Tiempo de servicio (años)</td>
                <td>
                    <input name="txtTiempoServicio" type="text" value="<?=$tiempoServicio?>" />
                </td>
            </tr>
            <tr>
                <td>Cargo</td>
                <td>
                    <select name="ddlCargo">
                        <option value="Administrador">Administrador</option>
                        <option value="Contador">Contador</option>
                        <option value="Empleado">Empleado</option>
                    </select>
                </td>
            </tr>
            <?php if ($montoUtilidades > 0) { ?>
            <tr>
                <td>Monto de utilidades</td>
                <td>
                    <input name="txtMontoUtilidades" type="text" class="TextoFondo" value="<?=$montoUtilidades?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="CALCULAR" />
