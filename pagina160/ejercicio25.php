<?php
// Variables
$genero = '';
$ocupacion = '';
$sueldo = 0;
$descuento = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $genero = $_POST["cmbGenero"];
    $ocupacion = $_POST["cmbOcupacion"];
    $sueldo = (float)$_POST["txtSueldo"];

    // Proceso
    if ($genero == 'hombre' && $ocupacion == 'obrero') {
        $descuento = $sueldo * 0.15;
    } elseif ($genero == 'hombre' && $ocupacion == 'empleado') {
        $descuento = $sueldo * 0.2;
    } elseif ($genero == 'mujer' && $ocupacion == 'obrera') {
        $descuento = $sueldo * 0.1;
    } elseif ($genero == 'mujer' && $ocupacion == 'empleada') {
        $descuento = $sueldo * 0.15;
    }
}

?>

<html>
<head>
    <title>Problema 25</title>
    <link rel="stylesheet" href="estilos25.css">
</head>
<body>
    <form method="post" action="ejercicio25.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 25</strong></td>
            </tr>
            <tr>
                <td>Género</td>
                <td>
                    <select name="cmbGenero">
                        <option value="hombre">Hombre</option>
                        <option value="mujer">Mujer</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Ocupación</td>
                <td>
                    <select name="cmbOcupacion">
                        <option value="obrero">Obrero</option>
                        <option value="empleado">Empleado</option>
                        <option value="obrera">Obrera</option>
                        <option value="empleada">Empleada</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Sueldo</td>
                <td>
                    <input name="txtSueldo" type="text" value="<?=$sueldo?>" />
                </td>
            </tr>
            <?php if ($descuento > 0) { ?>
            <tr>
                <td>Monto a descontar</td>
                <td>
                    <input name="txtDescuento" type="text" class="TextoFondo" value="<?=$descuento?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
