<?php
// Variables
$kilos = 0;
$precio = 0;
$totalPagar = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $kilos = (float)$_POST["txtKilos"];
    $precio = (float)$_POST["txtPrecio"];

    // Proceso
    if ($kilos >= 0 && $kilos <= 2) {
        $totalPagar = $kilos * $precio;
    } elseif ($kilos > 2 && $kilos <= 5) {
        $totalPagar = $kilos * $precio * 0.9; // 10% de descuento
    } elseif ($kilos > 5 && $kilos <= 10) {
        $totalPagar = $kilos * $precio * 0.8; // 20% de descuento
    } elseif ($kilos > 10) {
        $totalPagar = $kilos * $precio * 0.7; // 30% de descuento
    }
}

?>

<html>
<head>
    <title>Problema 26</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio26.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 26</strong></td>
            </tr>
            <tr>
                <td>Kilos de manzanas</td>
                <td>
                    <input name="txtKilos" type="text" value="<?=$kilos?>" />
                </td>
            </tr>
            <tr>
                <td>Precio por kilo</td>
                <td>
                    <input name="txtPrecio" type="text" value="<?=$precio?>" />
                </td>
            </tr>
            <?php if ($totalPagar > 0) { ?>
            <tr>
                <td>Total a pagar</td>
                <td>
                    <input name="txtTotalPagar" type="text" class="TextoFondo" value="<?=$totalPagar?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
