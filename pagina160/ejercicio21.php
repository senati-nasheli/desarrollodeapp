<?php
// Variables
$numeroMes = 0;
$nombreMes = '';

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $numeroMes = (int)$_POST["txtNumeroMes"];

    // Proceso
    switch ($numeroMes) {
        case 1:
            $nombreMes = 'Enero';
            break;
        case 2:
            $nombreMes = 'Febrero';
            break;
        case 3:
            $nombreMes = 'Marzo';
            break;
        case 4:
            $nombreMes = 'Abril';
            break;
        case 5:
            $nombreMes = 'Mayo';
            break;
        case 6:
            $nombreMes = 'Junio';
            break;
        case 7:
            $nombreMes = 'Julio';
            break;
        case 8:
            $nombreMes = 'Agosto';
            break;
        case 9:
            $nombreMes = 'Septiembre';
            break;
        case 10:
            $nombreMes = 'Octubre';
            break;
        case 11:
            $nombreMes = 'Noviembre';
            break;
        case 12:
            $nombreMes = 'Diciembre';
            break;
        default:
            $nombreMes = 'Mes inválido';
            break;
    }
}

?>

<html>
<head>
    <title>Problema 21</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio21.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 21</strong></td>
            </tr>
            <tr>
                <td>Número de mes</td>
                <td>
                    <input name="txtNumeroMes" type="text" id="txtNumeroMes" value="<?=$numeroMes?>" />
                </td>
            </tr>
            <?php if ($nombreMes != '') { ?>
            <tr>
                <td>Mes en letras</td>
                <td>
                    <input name="txtNombreMes" type="text" class="TextoFondo" id="txtNombreMes" value="<?=$nombreMes?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
