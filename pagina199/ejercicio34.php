<?php
// Variables
$numero = 0;
$cantidadCeros = 0;

if(isset($_POST["btnContar"])) {
    // Entrada
    $numero = (int)$_POST["txtNumero"];

    // Proceso
    $numeroString = (string)$numero;
    $cantidadCeros = substr_count($numeroString, '0');
}

?>

<html>
<head>
    <title>Problema 34</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio34.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 34</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtNumero" type="number" class="TextoFondo" id="txtNumero" value="<?=$numero?>" />
                </td>
            </tr>
            <?php if ($cantidadCeros > 0) { ?>
            <tr>
                <td>Cantidad de dígitos "0"</td>
                <td>
                    <input name="txtCantidadCeros" type="text" class="TextoFondo" id="txtCantidadCeros" value="<?=$cantidadCeros?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnContar" type="submit" id="btnContar" value="CONTAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
