<?php
// Variables
$numero = 0;
$factorial = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $numero = (int)$_POST["txtnumero"];

    // Cálculo del factorial
    if ($numero == 0 || $numero == 1) {
        $factorial = 1;
    } else {
        $factorial = 1;
        for ($i = 2; $i <= $numero; $i++) {
            $factorial *= $i;
        }
    }
}
?>

<html>
<head>
    <title>Problema 31</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio31.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 31</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtnumero" type="number" class="TextoFondo" id="txtnumero" value="<?=$numero?>" />
                </td>
            </tr>
            <?php if ($factorial > 0) { ?>
            <tr>
                <td>Factorial</td>
                <td>
                    <input name="txtfactorial" type="text" class="TextoFondo" id="txtfactorial" value="<?=$factorial?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
