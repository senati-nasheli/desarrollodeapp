<?php
// Función para calcular el MCD utilizando el algoritmo de Euclides
function calcularMCD($numero1, $numero2) {
    while ($numero2 != 0) {
        $resto = $numero1 % $numero2;
        $numero1 = $numero2;
        $numero2 = $resto;
    }
    return $numero1;
}

// Variables
$numero1 = 0;
$numero2 = 0;
$mcd = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $numero1 = $_POST["txtnumero1"];
    $numero2 = $_POST["txtnumero2"];

    // Proceso
    $mcd = calcularMCD($numero1, $numero2);
}
?>

<html>
<head>
    <title>Problema 39</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio39.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 39</strong></td>
            </tr>
            <tr>
                <td>Número 1</td>
                <td>
                    <input name="txtnumero1" type="number" id="txtnumero1" value="<?=$numero1?>" />
                </td>
            </tr>
            <tr>
                <td>Número 2</td>
                <td>
                    <input name="txtnumero2" type="number" id="txtnumero2" value="<?=$numero2?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular MCD" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Máximo Común Divisor (MCD)</td>
                    <td><?=$mcd?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
