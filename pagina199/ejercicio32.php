<?php
// Variables
$rangoInicial = 0;
$rangoFinal = 0;
$cantidadPares = 0;
$cantidadImpares = 0;

if (isset($_POST["btnContar"])) {
    // Entrada
    $rangoInicial = (int)$_POST["txtrangoInicial"];
    $rangoFinal = (int)$_POST["txtrangoFinal"];

    // Conteo de números pares e impares
    for ($numero = $rangoInicial; $numero <= $rangoFinal; $numero++) {
        if ($numero % 5 == 0) {
            continue; // Salta los múltiplos de 5
        }

        if ($numero % 2 == 0) {
            $cantidadPares++;
        } else {
            $cantidadImpares++;
        }
    }
}
?>

<html>
<head>
    <title>Problema 32</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio32.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 32</strong></td>
            </tr>
            <tr>
                <td>Ingrese el rango inicial</td>
                <td>
                    <input name="txtrangoInicial" type="number" class="TextoFondo" id="txtrangoInicial" value="<?=$rangoInicial?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el rango final</td>
                <td>
                    <input name="txtrangoFinal" type="number" class="TextoFondo" id="txtrangoFinal" value="<?=$rangoFinal?>" />
                </td>
            </tr>
            <?php if ($cantidadPares > 0 || $cantidadImpares > 0) { ?>
            <tr>
                <td>Cantidad de números pares</td>
                <td>
                    <input name="txtpares" type="text" class="TextoFondo" id="txtpares" value="<?=$cantidadPares?>" readonly />
                </td>
            </tr>
            <tr>
                <td>Cantidad de números impares</td>
                <td>
                    <input name="txtimpares" type="text" class="TextoFondo" id="txtimpares" value="<?=$cantidadImpares?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnContar" type="submit" id="btnContar" value="CONTAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
