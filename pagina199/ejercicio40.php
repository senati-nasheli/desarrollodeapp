<?php
// Función para obtener los factores primos de un número
function obtenerFactoresPrimos($numero) {
    $factores = array();
    $divisor = 2;

    while ($numero > 1) {
        if ($numero % $divisor == 0) {
            $factores[] = $divisor;
            $numero /= $divisor;
        } else {
            $divisor++;
        }
    }

    return $factores;
}

// Función para calcular el MCD utilizando el método de factorización simultánea
function calcularMCD($numero1, $numero2) {
    $factores1 = obtenerFactoresPrimos($numero1);
    $factores2 = obtenerFactoresPrimos($numero2);
    $mcd = 1;

    foreach ($factores1 as $factor) {
        if (in_array($factor, $factores2)) {
            $mcd *= $factor;
            $factores2 = array_diff($factores2, array($factor));
        }
    }

    return $mcd;
}

// Variables
$numero1 = 0;
$numero2 = 0;
$mcd = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $numero1 = $_POST["txtnumero1"];
    $numero2 = $_POST["txtnumero2"];

    // Proceso
    $mcd = calcularMCD($numero1, $numero2);
}
?>

<html>
<head>
    <title>Problema 40</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio40.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 40</strong></td>
            </tr>
            <tr>
                <td>Número 1</td>
                <td>
                    <input name="txtnumero1" type="number" id="txtnumero1" value="<?=$numero1?>" />
                </td>
            </tr>
            <tr>
                <td>Número 2</td>
                <td>
                    <input name="txtnumero2" type="number" id="txtnumero2" value="<?=$numero2?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular MCD" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Máximo Común Divisor (MCD)</td>
                    <td><?=$mcd?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
