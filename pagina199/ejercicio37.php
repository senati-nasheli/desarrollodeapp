<?php
// Función para verificar si un número es primo
function esPrimo($numero) {
    if ($numero < 2) {
        return false;
    }

    for ($i = 2; $i <= sqrt($numero); $i++) {
        if ($numero % $i === 0) {
            return false;
        }
    }

    return true;
}

// Variables
$rangoInicial = 0;
$rangoFinal = 0;
$cantidadPrimos = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $rangoInicial = $_POST["txtrangoInicial"];
    $rangoFinal = $_POST["txtrangoFinal"];

    // Proceso
    $cantidadPrimos = 0;

    for ($i = $rangoInicial; $i <= $rangoFinal; $i++) {
        if (esPrimo($i)) {
            $cantidadPrimos++;
        }
    }
}
?>

<html>
<head>
    <title>Problema 37</title>
    <link rel="stylesheet" href="estilos.css">

</head>
<body>
    <form method="post" action="ejercicio37.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 37</strong></td>
            </tr>
            <tr>
                <td>Rango inicial</td>
                <td>
                    <input name="txtrangoInicial" type="number" id="txtrangoInicial" value="<?=$rangoInicial?>" />
                </td>
            </tr>
            <tr>
                <td>Rango final</td>
                <td>
                    <input name="txtrangoFinal" type="number" id="txtrangoFinal" value="<?=$rangoFinal?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular Cantidad" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Cantidad de números primos</td>
                    <td><?=$cantidadPrimos?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
