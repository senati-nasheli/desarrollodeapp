<?php
// Función para verificar si un número es capicúa
function esCapicua($numero) {
    $numeroString = (string) $numero;
    $longitud = strlen($numeroString);

    for ($i = 0; $i < $longitud / 2; $i++) {
        if ($numeroString[$i] !== $numeroString[$longitud - $i - 1]) {
            return false;
        }
    }

    return true;
}

// Variables
$rangoInicial = 0;
$rangoFinal = 0;
$cantidadCapicuas = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $rangoInicial = $_POST["txtrangoInicial"];
    $rangoFinal = $_POST["txtrangoFinal"];

    // Proceso
    $cantidadCapicuas = 0;

    for ($i = $rangoInicial; $i <= $rangoFinal; $i++) {
        if (esCapicua($i)) {
            $cantidadCapicuas++;
        }
    }
}
?>

<html>
<head>
    <title>Problema 38</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio38.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 38</strong></td>
            </tr>
            <tr>
                <td>Rango inicial</td>
                <td>
                    <input name="txtrangoInicial" type="number" id="txtrangoInicial" value="<?=$rangoInicial?>" />
                </td>
            </tr>
            <tr>
                <td>Rango final</td>
                <td>
                    <input name="txtrangoFinal" type="number" id="txtrangoFinal" value="<?=$rangoFinal?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular Cantidad" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Cantidad de números capicúa</td>
                    <td><?=$cantidadCapicuas?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
