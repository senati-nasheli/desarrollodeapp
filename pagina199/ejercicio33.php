<?php
// Variables
$numero = 0;
$suma = 0;
$producto = 1;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $numero = (int)$_POST["txtNumero"];

    // Proceso
    for($i = 1; $i <= $numero; $i++) {
        $multiplo = $i * 3;
        $suma += $multiplo;
        $producto *= $multiplo;
    }
}

?>

<html>
<head>
    <title>Problema 33</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio33.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 33</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtNumero" type="number" class="TextoFondo" id="txtNumero" value="<?=$numero?>" />
                </td>
            </tr>
            <?php if ($suma > 0 && $producto > 0) { ?>
            <tr>
                <td>Suma de los múltiplos de 3</td>
                <td>
                    <input name="txtSuma" type="text" class="TextoFondo" id="txtSuma" value="<?=$suma?>" readonly />
                </td>
            </tr>
            <tr>
                <td>Producto de los múltiplos de 3</td>
                <td>
                    <input name="txtProducto" type="text" class="TextoFondo" id="txtProducto" value="<?=$producto?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

