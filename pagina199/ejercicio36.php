<?php
// Variables
$numero = 0;
$totalNumeros = 0;
$porcentajePares = 0;
$porcentajeImpares = 0;
$porcentajeNeutros = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $numero = $_POST["txtnumero"];

    // Proceso
    $totalNumeros = strlen($numero); // Obtenemos la cantidad de dígitos del número

    $cantidadPares = 0;
    $cantidadImpares = 0;
    $cantidadNeutros = 0;

    for($i = 0; $i < $totalNumeros; $i++) {
        $digito = $numero[$i];

        if($digito == 0) {
            $cantidadNeutros++;
        } elseif($digito % 2 == 0) {
            $cantidadPares++;
        } else {
            $cantidadImpares++;
        }
    }

    $porcentajePares = ($cantidadPares / $totalNumeros) * 100;
    $porcentajeImpares = ($cantidadImpares / $totalNumeros) * 100;
    $porcentajeNeutros = ($cantidadNeutros / $totalNumeros) * 100;
}
?>

<html>
<head>
    <title>Problema 36</title>
    <link rel="stylesheet" href="estilos.css">

</head>
<body>
    <form method="post" action="ejercicio36.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 36</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtnumero" type="number" id="txtnumero" value="<?=$numero?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular Porcentaje" />
                </td>
            </tr>
            <?php if(isset($_POST["btnCalcular"])) { ?>
            <tr>
                <td>Pares</td>
                <td><?=$porcentajePares?>%</td>
            </tr>
            <tr>
                <td>Impares</td>
                <td><?=$porcentajeImpares?>%</td>
            </tr>
            <tr>
                <td>Neutros (0)</td>
                <td><?=$porcentajeNeutros?>%</td>
            </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
