<?php
// Variables
$numero = 0;
$digito = 0;
$existeDigito = false;

if(isset($_POST["btnBuscar"])) {
    // Entrada
    $numero = $_POST["txtnumero"];
    $digito = $_POST["txtdigito"];

    // Proceso
    $numeroAbsoluto = abs($numero); 
    $numeroString = (string)$numeroAbsoluto; // Convertimos el número a string para poder recorrer cada dígito
    $existeDigito = false;

    for($i = 0; $i < strlen($numeroString); $i++) {
        if($numeroString[$i] == $digito) {
            $existeDigito = true;
            break;
        }
    }
}
?>

<html>
<head>
    <title>Problema 35</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="ejercicio35.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 35</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtnumero" type="number" id="txtnumero" value="<?=$numero?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese un dígito</td>
                <td>
                    <input name="txtdigito" type="number" id="txtdigito" value="<?=$digito?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnBuscar" type="submit" id="btnBuscar" value="Buscar Dígito" />
                </td>
            </tr>
            <tr>
                <td>El dígito <?=$digito?> <?=$existeDigito ? "existe" : "no existe"?> en el número <?=$numero?></td>
            </tr>
        </table>
    </form>
</body>
</html>
