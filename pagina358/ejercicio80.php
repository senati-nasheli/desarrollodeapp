<?php
function MultiplicarMatrices($matrizA, $matrizB) {
    $filasA = count($matrizA);
    $columnasA = count($matrizA[0]);
    $filasB = count($matrizB);
    $columnasB = count($matrizB[0]);

    // Verificar si las matrices son multiplicables
    if ($columnasA != $filasB) {
        return null; // No se pueden multiplicar las matrices
    }

    // Inicializar la matriz resultado C con ceros
    $matrizC = array();
    for ($i = 0; $i < $filasA; $i++) {
        $matrizC[$i] = array();
        for ($j = 0; $j < $columnasB; $j++) {
            $matrizC[$i][$j] = 0;
        }
    }

    // Realizar la multiplicación de las matrices
    for ($i = 0; $i < $filasA; $i++) {
        for ($j = 0; $j < $columnasB; $j++) {
            for ($k = 0; $k < $columnasA; $k++) {
                $matrizC[$i][$j] += $matrizA[$i][$k] * $matrizB[$k][$j];
            }
        }
    }

    return $matrizC;
}

$matrizA = array();
$matrizB = array();

if (isset($_POST["btnCalcular"])) {
    // Obtener los valores ingresados por el usuario para la matriz A
    $matrizA[0][0] = $_POST["txtA11"];
    $matrizA[0][1] = $_POST["txtA12"];
    $matrizA[1][0] = $_POST["txtA21"];
    $matrizA[1][1] = $_POST["txtA22"];

    // Obtener los valores ingresados por el usuario para la matriz B
    $matrizB[0][0] = $_POST["txtB11"];
    $matrizB[0][1] = $_POST["txtB12"];
    $matrizB[1][0] = $_POST["txtB21"];
    $matrizB[1][1] = $_POST["txtB22"];

    $matrizC = MultiplicarMatrices($matrizA, $matrizB);
}
?>

<html>
<head>
    <title>Problema 80</title>
    <link rel="stylesheet" href="estilos80.css">
</head>
<body>
    <form method="post" action="ejercicio80.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 80</strong></td>
            </tr>
            <tr>
                <td>Matriz A</td>
                <td>
                    <input name="txtA11" type="number" value="<?= $_POST["txtA11"] ?? '' ?>" required />
                    <input name="txtA12" type="number" value="<?= $_POST["txtA12"] ?? '' ?>" required/>
                    <br>
                    <input name="txtA21" type="number" value="<?= $_POST["txtA21"] ?? '' ?>" required />
                    <input name="txtA22" type="number" value="<?= $_POST["txtA22"] ?? '' ?>" required />
                </td>
            </tr>
            <tr>
                <td>Matriz B</td>
                <td>
                    <input name="txtB11" type="number" value="<?= $_POST["txtB11"] ?? '' ?>" required/>
                    <input name="txtB12" type="number" value="<?= $_POST["txtB12"] ?? '' ?>" required/>
                    <br>
                    <input name="txtB21" type="number" value="<?= $_POST["txtB21"] ?? '' ?>" required />
                    <input name="txtB22" type="number" value="<?= $_POST["txtB22"] ?? '' ?>" required/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"]) && $matrizC !== null) { ?>
                <tr>
                    <td>Matriz C = A * B</td>
                    <td>
                        <?= $matrizC[0][0] ?> <?= $matrizC[0][1] ?><br>
                        <?= $matrizC[1][0] ?> <?= $matrizC[1][1] ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>

