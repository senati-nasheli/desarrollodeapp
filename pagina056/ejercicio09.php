<?php
// Variables
$base = 0;
$altura = 0;
$area = 0;
$perimetro = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $base = (float)$_POST["txtbase"];
    $altura = (float)$_POST["txtaltura"];

    // Proceso
    $area = $base * $altura;
    $perimetro = 2 * $base + 2 * $altura;
}

?>

<html>
<head>
    <title>Problema 9</title>
    <link rel="stylesheet" href="estilos09.css">
</head>
<body>
    <form method="post" action="ejercicio09.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 9</strong></td>
            </tr>
            <tr>
                <td>Ingrese la base</td>
                <td>
                    <input name="txtbase" type="text" id="txtbase" value="<?=$base?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese la altura</td>
                <td>
                    <input name="txtaltura" type="text" id="txtaltura" value="<?=$altura?>" />
                </td>
            </tr>
            <?php if ($area > 0 && $perimetro > 0) { ?>
            <tr>
                <td>Área del rectangulo</td>
                <td>
                    <input name="txtarea" type="text" class="TextoFondo" id="txtarea" value="<?=$area?>" />
                </td>
            </tr>
            <tr>
                <td>Perímetro del rectangulo</td>
                <td>
                    <input name="txtperimetro" type="text" class="TextoFondo" id="txtperimetro" value="<?=$perimetro?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
