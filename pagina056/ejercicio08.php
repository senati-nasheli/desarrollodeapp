<?php
// Variables
$fahrenheit = 0;
$celsius = 0;
$kelvin = 0;

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $fahrenheit = (float)$_POST["txtfahrenheit"];

    // Proceso
    $celsius = ($fahrenheit - 32) * 5/9;
    $kelvin = $celsius + 273.15;
}

?>

<html>
<head>
    <title>Problema 8</title>
    <link rel="stylesheet" href="estilos08.css">
</head>
<body>
    <form method="post" action="ejercicio08.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 8</strong></td>
            </tr>
            <tr>
                <td>Ingrese la cantidad de grados Fahrenheit</td>
                <td>
                    <input name="txtfahrenheit" type="text" id="txtfahrenheit" value="<?=$fahrenheit?>" />
                </td>
            </tr>
            <?php if ($celsius != 0) { ?>
            <tr>
                <td>Equivalente en grados Celsius</td>
                <td>
                    <input name="txtcelsius" type="text" class="TextoFondo" id="txtcelsius" value="<?=$celsius?>" />
                </td>
            </tr>
            <?php } ?>
            <?php if ($kelvin != 0) { ?>
            <tr>
                <td>Equivalente en grados Kelvin</td>
                <td>
                    <input name="txtkelvin" type="text" class="TextoFondo" id="txtkelvin" value="<?=$kelvin?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
