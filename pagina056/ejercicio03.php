<?php
//Variables
$mm = 0;
$m = 0;
$dm = 0;
$cm = 0;

if(isset($_POST["btnCalcular"])) {
    //Entrada
    $mm = (float)$_POST["txtmm"];
    //Proceso
    $m = floor($mm / 1000);
    $mm_restantes = $mm % 1000;
    $dm = floor($mm_restantes / 100);
    $dm_restantes = $mm_restantes % 100;
    $cm = floor($dm_restantes / 10);
    $mm_final = $dm_restantes % 10;
}
?>


<html>
    <link rel="stylesheet" href="estilos03.css">

<head>
    <title>Document</title>
</head>
<body>
    <form method="post" action="ejercicio03.php">
    <table width="241" border="0">
    <tr>
        <td colspan="2"><strong>Ejercicio 03</strong> </td>
    </tr>
    <tr>
        <td width="81">cantidad en mm</td>
        <td width="150">
         <input name="txtmm" type="text" id="txtmm" value="<?=$mm?>" />
    </td>
    </tr>
    <tr>
        <td>En metros</td>
        <td>
            <input name="txts" type="text" class="TextoFondo" id="txts"
            value="<?=$m?>"/>
        </td>
    </tr>
    <tr>
        <td>En decimetros</td>
        <td>
            <input name="txts" type="text" class="TextoFondo" id="txts"
            value="<?=$dm?>"/>
        </td>
    </tr><tr>
        <td>En centimetros</td>
        <td>
            <input name="txts" type="text" class="TextoFondo" id="txts"
            value="<?=$cm?>"/>
        </td>
    </tr>
   
    <tr>
        <td>&nbsp;</td>
        <td>
            <input name="btnCalcular" type="submit" id="btnCalcular"
            value="CALCULAR" />
        </td>
    </tr>
    </table>
</form>
</body>
</html>