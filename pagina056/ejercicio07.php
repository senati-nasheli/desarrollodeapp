<?php
// Variables
$horas = 0;
$minutos = 0;
$segundos = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $horas = (int)$_POST["txthoras"];

    // Proceso
    $minutos = $horas * 60;
    $segundos = $horas * 3600;
}
?>

<html>
<head>
    <title>Problema 7</title>
    <link rel="stylesheet" href="estilos07.css">
</head>
<body>
    <form method="post" action="ejercicio07.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 7</strong></td>
            </tr>
            <tr>
                <td>Ingrese la cantidad de horas</td>
                <td>
                    <input name="txthoras" type="text" id="txthoras" value="<?=$horas?>" />
                </td>
            </tr>
            <?php if ($minutos > 0) { ?>
            <tr>
                <td>Equivalente en minutos</td>
                <td>
                    <input name="txtminutos" type="text" class="TextoFondo" id="txtminutos" value="<?=$minutos?>" />
                </td>
            </tr>
            <?php } ?>
            <?php if ($segundos > 0) { ?>
            <tr>
                <td>Equivalente en segundos</td>
                <td>
                    <input name="txtsegundos" type="text" class="TextoFondo" id="txtsegundos" value="<?=$segundos?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
