<?php
// Variables
$grados = 0;
$minutos = 0;
$segundos = 0;
$centesimales = 0;

if(isset($_POST["btnConvertir"])) {
    // Entrada
    $grados = (int)$_POST["txtgrados"];
    $minutos = (int)$_POST["txtminutos"];
    $segundos = (int)$_POST["txtsegundos"];

    // Proceso
    $grados_decimales = $grados + ($minutos / 60) + ($segundos / 3600);
    $centesimales = $grados_decimales * 100;

}

?>

<html>
<head>
    <title>Problema 10</title>
    <link rel="stylesheet" href="estilos10.css">
</head>
<body>
    <form method="post" action="ejercicio10.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 10</strong></td>
            </tr>
            <tr>
                <td>Ingrese los grados</td>
                <td>
                    <input name="txtgrados" type="text" id="txtgrados" value="<?=$grados?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese los minutos</td>
                <td>
                    <input name="txtminutos" type="text" id="txtminutos" value="<?=$minutos?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese los segundos</td>
                <td>
                    <input name="txtsegundos" type="text" id="txtsegundos" value="<?=$segundos?>" />
                </td>
            </tr>
            <?php if ($centesimales > 0) { ?>
            <tr>
                <td>Grados centesimales</td>
                <td>
                    <input name="txtcentesimales" type="text" class="TextoFondo" id="txtcentesimales" value="<?=$centesimales?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnConvertir" type="submit" id="btnConvertir" value="CONVERTIR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
