<?php
// Variables
$lado = 0;
$area = 0;
$perimetro = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $lado = (float)$_POST["txtlado"];

    // Proceso
    $area = pow($lado, 2);
    $perimetro = 4 * $lado;
}
?>

<html>
<head>
    <title>Problema 6</title>
    <link rel="stylesheet" href="estilos06.css">
</head>
<body>
    <form method="post" action="ejercicio06.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 6</strong></td>
            </tr>
            <tr>
                <td>Ingrese la medida del lado</td>
                <td>
                    <input name="txtlado" type="text" id="txtlado" value="<?=$lado?>" />
                </td>
            </tr>
            <?php if ($area > 0) { ?>
            <tr>
                <td>Área del cuadrado</td>
                <td>
                    <input name="txtarea" type="text" class="TextoFondo" id="txtarea" value="<?=$area?>" />
                </td>
            </tr>
            <?php } ?>
            <?php if ($perimetro > 0) { ?>
            <tr>
                <td>Perímetro del cuadrado</td>
                <td>
                    <input name="txtperimetro" type="text" class="TextoFondo" id="txtperimetro" value="<?=$perimetro?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
