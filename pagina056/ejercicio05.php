<?php
// Variables
$num1 = 0;
$num2 = 0;
$num3 = 0;
$num4 = 0;
$suma = 0;
$porc1 = 0;
$porc2 = 0;
$porc3 = 0;
$porc4 = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $num1 = (int)$_POST["txtnum1"];
    $num2 = (int)$_POST["txtnum2"];
    $num3 = (int)$_POST["txtnum3"];
    $num4 = (int)$_POST["txtnum4"];

    // Proceso
    $suma = $num1 + $num2 + $num3 + $num4;

    if ($suma != 0) {
        $porc1 = ($num1 / $suma) * 100;
        $porc2 = ($num2 / $suma) * 100;
        $porc3 = ($num3 / $suma) * 100;
        $porc4 = ($num4 / $suma) * 100;
    }
}

?>

<html>
<head>
    <title>Problema 5</title>
    <link rel="stylesheet" href="estilos05.css">
</head>
<body>
    <form method="post" action="ejercicio05.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 5</strong></td>
            </tr>
            <tr>
                <td>Primer número</td>
                <td>
                    <input name="txtnum1" type="text" id="txtnum1" value="<?=$num1?>" />
                </td>
            </tr>
            <tr>
                <td>Segundo número</td>
                <td>
                    <input name="txtnum2" type="text" id="txtnum2" value="<?=$num2?>" />
                </td>
            </tr>
            <tr>
                <td>Tercer número</td>
                <td>
                    <input name="txtnum3" type="text" id="txtnum3" value="<?=$num3?>" />
                </td>
            </tr>
            <tr>
                <td>Cuarto número</td>
                <td>
                    <input name="txtnum4" type="text" id="txtnum4" value="<?=$num4?>" />
                </td>
            </tr>
            <tr>
                <td>Porcentaje del primer número</td>
                <td>
                    <input name="txtporcentaje1" type="text" class="TextoFondo" id="txtporcentaje1" value="<?=$porc1?>" />
                </td>
            </tr>
            <tr>
                <td>Porcentaje del segundo número</td>
                <td>
                    <input name="txtporcentaje2" type="text" class="TextoFondo" id="txtporcentaje2" value="<?=$porc2?>" />
                </td>
            </tr>
            <tr>
                <td>Porcentaje del tercer número</td>
                <td>
                    <input name="txtporcentaje3" type="text" class="TextoFondo" id="txtporcentaje3" value="<?=$porc3?>" />
                </td>
            </tr>
            <tr>
                <td>Porcentaje del cuarto número</td>
                <td>
                    <input name="txtporcentaje4" type="text" class="TextoFondo" id="txtporcentaje4" value="<?=$porc4?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>