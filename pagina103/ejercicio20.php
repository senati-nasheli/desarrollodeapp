<?php
// Variables
$lado1 = 0;
$lado2 = 0;
$lado3 = 0;
$tipoTriangulo = '';

if(isset($_POST["btnVerificar"])) {
    // Entrada
    $lado1 = (float)$_POST["txtLado1"];
    $lado2 = (float)$_POST["txtLado2"];
    $lado3 = (float)$_POST["txtLado3"];

    // Proceso
    if ($lado1 == $lado2 && $lado2 == $lado3) {
        $tipoTriangulo = 'Triángulo equilátero';
    } elseif ($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3) {
        $tipoTriangulo = 'Triángulo isósceles';
    } else {
        $tipoTriangulo = 'Triángulo escaleno';
    }
}

?>

<html>
<head>
    <title>Problema 20</title>
    <link rel="stylesheet" href="estilos20.css">
</head>
<body>
    <form method="post" action="ejercicio20.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 20</strong></td>
            </tr>
            <tr>
                <td>Lado 1</td>
                <td>
                    <input name="txtLado1" type="text" id="txtLado1" value="<?=$lado1?>" />
                </td>
            </tr>
            <tr>
                <td>Lado 2</td>
                <td>
                    <input name="txtLado2" type="text" id="txtLado2" value="<?=$lado2?>" />
                </td>
            </tr>
            <tr>
                <td>Lado 3</td>
                <td>
                    <input name="txtLado3" type="text" id="txtLado3" value="<?=$lado3?>" />
                </td>
            </tr>
            <?php if ($tipoTriangulo != '') { ?>
            <tr>
                <td>Tipo de triángulo</td>
                <td>
                    <input name="txtTipoTriangulo" type="text" class="TextoFondo" id="txtTipoTriangulo" value="<?=$tipoTriangulo?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" id="btnVerificar" value="VERIFICAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
