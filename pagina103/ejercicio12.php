<?php
// Variables
$numero1 = 0;
$numero2 = 0;
$menor = 0;

if(isset($_POST["btnObtenerMenor"])) {
    // Entrada
    $numero1 = (int)$_POST["txtNumero1"];
    $numero2 = (int)$_POST["txtNumero2"];

    // Proceso
    $menor = min($numero1, $numero2);
}

?>

<html>
<head>
    <title>Problema 12</title>
    <link rel="stylesheet" href="estilos12.css">
</head>
<body>
    <form method="post" action="ejercicio12.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 12</strong></td>
            </tr>
            <tr>
                <td>Ingrese el primer número</td>
                <td>
                    <input name="txtNumero1" type="text" id="txtNumero1" value="<?=$numero1?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el segundo número</td>
                <td>
                    <input name="txtNumero2" type="text" id="txtNumero2" value="<?=$numero2?>" />
                </td>
            </tr>
            <?php if ($menor > 0) { ?>
            <tr>
                <td>Número menor</td>
                <td>
                    <input name="txtMenor" type="text" class="TextoFondo" id="txtMenor" value="<?=$menor?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnObtenerMenor" type="submit" id="btnObtenerMenor" value="OBTENER MENOR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
