<?php
// Variables
$lado1 = 0;
$lado2 = 0;
$lado3 = 0;
$resultado = '';

if(isset($_POST["btnVerificar"])) {
    // Entrada
    $lado1 = (float)$_POST["txtLado1"];
    $lado2 = (float)$_POST["txtLado2"];
    $lado3 = (float)$_POST["txtLado3"];

    // Proceso
    if ($lado1 < ($lado2 + $lado3) && $lado2 < ($lado1 + $lado3) && $lado3 < ($lado1 + $lado2)) {
        $resultado = 'Los lados forman un triángulo.';
    } else {
        $resultado = 'Los lados no forman un triángulo.';
    }
}

?>

<html>
<head>
    <title>Problema 19</title>
    <link rel="stylesheet" href="estilos19.css">
</head>
<body>
    <form method="post" action="ejercicio19.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 19</strong></td>
            </tr>
            <tr>
                <td>Lado 1</td>
                <td>
                    <input name="txtLado1" type="text" id="txtLado1" value="<?=$lado1?>" />
                </td>
            </tr>
            <tr>
                <td>Lado 2</td>
                <td>
                    <input name="txtLado2" type="text" id="txtLado2" value="<?=$lado2?>" />
                </td>
            </tr>
            <tr>
                <td>Lado 3</td>
                <td>
                    <input name="txtLado3" type="text" id="txtLado3" value="<?=$lado3?>" />
                </td>
            </tr>
            <?php if ($resultado != '') { ?>
            <tr>
                <td>Resultado</td>
                <td>
                    <input name="txtResultado" type="text" class="TextoFondo" id="txtResultado" value="<?=$resultado?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" id="btnVerificar" value="VERIFICAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
