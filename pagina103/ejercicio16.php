<?php
// Variables
$nota1 = 0;
$nota2 = 0;
$nota3 = 0;
$nota4 = 0;
$promedio = 0;
$mensaje = "";

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $nota1 = (float)$_POST["txtNota1"];
    $nota2 = (float)$_POST["txtNota2"];
    $nota3 = (float)$_POST["txtNota3"];
    $nota4 = (float)$_POST["txtNota4"];

    // Proceso
    // Ordenar las notas de mayor a menor
    $notas = array($nota1, $nota2, $nota3, $nota4);
    rsort($notas);

    // Calcular el promedio de las tres mejores notas
    $promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;

    // Determinar el mensaje según el promedio
    if ($promedio >= 11) {
        $mensaje = "Aprobado";
    } else {
        $mensaje = "Desaprobado";
    }
}

?>

<html>
<head>
    <title>Problema 16</title>
    <link rel="stylesheet" href="estilos16.css">
</head>
<body>
    <form method="post" action="ejercicio16.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 16</strong></td>
            </tr>
            <tr>
                <td>Ingrese la nota 1</td>
                <td>
                    <input name="txtNota1" type="text" id="txtNota1" value="<?=$nota1?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese la nota 2</td>
                <td>
                    <input name="txtNota2" type="text" id="txtNota2" value="<?=$nota2?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese la nota 3</td>
                <td>
                    <input name="txtNota3" type="text" id="txtNota3" value="<?=$nota3?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese la nota 4</td>
                <td>
                    <input name="txtNota4" type="text" id="txtNota4" value="<?=$nota4?>" />
                </td>
            </tr>
            <?php if (!empty($promedio) && !empty($mensaje)) { ?>
            <tr>
                <td>Promedio de las tres mejores notas</td>
                <td>
                    <input name="txtPromedio" type="text" class="TextoFondo" id="txtPromedio" value="<?=$promedio?>" readonly />
                </td>
            </tr>
            <tr>
                <td>Mensaje</td>
                <td>
                    <input name="txtMensaje" type="text" class="TextoFondo" id="txtMensaje" value="<?=$mensaje?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
            <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>