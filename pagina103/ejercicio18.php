<?php
// Variables
$a = 0;
$b = 0;
$resultado = '';

if(isset($_POST["btnComparar"])) {
    // Entrada
    $a = (int)$_POST["txtNumeroA"];
    $b = (int)$_POST["txtNumeroB"];

    // Proceso
    if ($a > $b) {
        $resultado = 'a es mayor que b';
    } elseif ($b > $a) {
        $resultado = 'b es mayor que a';
    } else {
        $resultado = 'a es igual a b';
    }
}

?>

<html>
<head>
    <title>Problema 18</title>
    <link rel="stylesheet" href="estilos18.css">
</head>
<body>
    <form method="post" action="ejercicio18.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 18</strong></td>
            </tr>
            <tr>
                <td>Número a</td>
                <td>
                    <input name="txtNumeroA" type="text" id="txtNumeroA" value="<?=$a?>" />
                </td>
            </tr>
            <tr>
                <td>Número b</td>
                <td>
                    <input name="txtNumeroB" type="text" id="txtNumeroB" value="<?=$b?>" />
                </td>
            </tr>
            <?php if ($resultado != '') { ?>
            <tr>
                <td>Resultado</td>
                <td>
                    <input name="txtResultado" type="text" class="TextoFondo" id="txtResultado" value="<?=$resultado?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnComparar" type="submit" id="btnComparar" value="COMPARAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
