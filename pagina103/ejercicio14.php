<?php
// Variables
$numero = 0;
$resultado = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $numero = (int)$_POST["txtNumero"];

    // Proceso
    if ($numero > 0) {
        $resultado = $numero * 2;
    } elseif ($numero < 0) {
        $resultado = $numero * 3;
    } else {
        $resultado = 0;
    }
}

?>

<html>
<head>
    <title>Problema 14</title>
    <link rel="stylesheet" href="estilos14.css">
</head>
<body>
    <form method="post" action="ejercicio14.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 14</strong></td>
            </tr>
            <tr>
                <td>Ingrese un número</td>
                <td>
                    <input name="txtNumero" type="text" id="txtNumero" value="<?=$numero?>" />
                </td>
            </tr>
            <?php if ($resultado != 0) { ?>
            <tr>
                <td>Resultado</td>
                <td>
                    <input name="txtResultado" type="text" class="TextoFondo" id="txtResultado" value="<?=$resultado?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
