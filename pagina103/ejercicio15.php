<?php
// Variables
$numero1 = 0;
$numero2 = 0;
$numero3 = 0;
$ascendente = "";
$descendente = "";

if(isset($_POST["btnOrdenar"])) {
    // Entrada
    $numero1 = (int)$_POST["txtNumero1"];
    $numero2 = (int)$_POST["txtNumero2"];
    $numero3 = (int)$_POST["txtNumero3"];

    // Proceso
    // Orden ascendente
    $ascendente = array($numero1, $numero2, $numero3);
    sort($ascendente);

    // Orden descendente
    $descendente = array($numero1, $numero2, $numero3);
    rsort($descendente);
}

?>

<html>
<head>
    <title>Problema 15</title>
    <link rel="stylesheet" href="estilos15.css">
</head>
<body>
    <form method="post" action="ejercicio15.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 15</strong></td>
            </tr>
            <tr>
                <td>Ingrese el primer número</td>
                <td>
                    <input name="txtNumero1" type="text" id="txtNumero1" value="<?=$numero1?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el segundo número</td>
                <td>
                    <input name="txtNumero2" type="text" id="txtNumero2" value="<?=$numero2?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el tercer número</td>
                <td>
                    <input name="txtNumero3" type="text" id="txtNumero3" value="<?=$numero3?>" />
                </td>
            </tr>
            <?php if (!empty($ascendente) && !empty($descendente)) { ?>
            <tr>
                <td>Números ordenados (ascendente)</td>
                <td>
                    <input name="txtAscendente" type="text" class="TextoFondo" id="txtAscendente" value="<?=implode(", ", $ascendente)?>" readonly />
                </td>
            </tr>
            <tr>
                <td>Números ordenados (descendente)</td>
                <td>
                    <input name="txtDescendente" type="text" class="TextoFondo" id="txtDescendente" value="<?=implode(", ", $descendente)?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnOrdenar" type="submit" id="btnOrdenar" value="ORDENAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
