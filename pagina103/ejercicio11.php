<?php
// Variable
$edad = 0;
$resultado = "";

if(isset($_POST["btnVerificar"])) {
    // Entrada
    $edad = (int)$_POST["txtEdad"];

    // Proceso
    if ($edad >= 18) {
        $resultado = "Es mayor de edad";
    } else {
        $resultado = "Es menor de edad";
    }
}

?>

<html>
<head>
    <title>Problema 11</title>
    <link rel="stylesheet" href="estilos11.css">
</head>
<body>
    <form method="post" action="ejercicio11.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 11</strong></td>
            </tr>
            <tr>
                <td>Ingrese la edad</td>
                <td>
                    <input name="txtEdad" type="text" id="txtEdad" value="<?=$edad?>" />
                </td>
            </tr>
            <?php if ($resultado != "") { ?>
            <tr>
                <td>Resultado</td>
                <td>
                    <input name="txtResultado" type="text" class="TextoFondo" id="txtResultado" value="<?=$resultado?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" id="btnVerificar" value="VERIFICAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
