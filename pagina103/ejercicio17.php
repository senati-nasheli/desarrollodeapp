<?php
// Variables
$saldo_anterior = 0;
$tipo_movimiento = '';
$monto_transaccion = 0;
$saldo_actual = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $saldo_anterior = (float)$_POST["txtSaldoAnterior"];
    $tipo_movimiento = $_POST["txtTipoMovimiento"];
    $monto_transaccion = (float)$_POST["txtMontoTransaccion"];

    // Proceso
    if ($tipo_movimiento == 'R') {
        $saldo_actual = $saldo_anterior - $monto_transaccion;
    } elseif ($tipo_movimiento == 'D') {
        $saldo_actual = $saldo_anterior + $monto_transaccion;
    }
}

?>

<html>
<head>
    <title>Problema 17</title>
    <link rel="stylesheet" href="estilos17.css">
</head>
<body>
    <form method="post" action="ejercicio17.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 17</strong></td>
            </tr>
            <tr>
                <td>Saldo anterior</td>
                <td>
                    <input name="txtSaldoAnterior" type="text" id="txtSaldoAnterior" value="<?=$saldo_anterior?>" />
                </td>
            </tr>
            <tr>
                <td>Tipo de movimiento</td>
                <td>
                    <input name="txtTipoMovimiento" type="text" id="txtTipoMovimiento" value="<?=$tipo_movimiento?>" />
                </td>
            </tr>
            <tr>
                <td>Monto de la transacción</td>
                <td>
                    <input name="txtMontoTransaccion" type="text" id="txtMontoTransaccion" value="<?=$monto_transaccion?>" />
                </td>
            </tr>
            <?php if ($saldo_actual > 0) { ?>
            <tr>
                <td>Saldo actual</td>
                <td>
                    <input name="txtSaldoActual" type="text" class="TextoFondo" id="txtSaldoActual" value="<?=$saldo_actual?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
