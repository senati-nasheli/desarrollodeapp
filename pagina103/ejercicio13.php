<?php
// Variables
$numero1 = 0;
$numero2 = 0;
$mensaje = "";

if(isset($_POST["btnVerificar"])) {
    // Entrada
    $numero1 = (int)$_POST["txtNumero1"];
    $numero2 = (int)$_POST["txtNumero2"];

    // Proceso
    if ($numero1 == $numero2) {
        $mensaje = "Los números son iguales";
    } else {
        $mensaje = "Los números son diferentes";
    }
}

?>

<html>
<head>
    <title>Problema 13</title>
    <link rel="stylesheet" href="estilos13.css">
</head>
<body>
    <form method="post" action="ejercicio13.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 13</strong></td>
            </tr>
            <tr>
                <td>Ingrese el primer número</td>
                <td>
                    <input name="txtNumero1" type="text" id="txtNumero1" value="<?=$numero1?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el segundo número</td>
                <td>
                    <input name="txtNumero2" type="text" id="txtNumero2" value="<?=$numero2?>" />
                </td>
            </tr>
            <?php if (!empty($mensaje)) { ?>
            <tr>
                <td>Resultado</td>
                <td>
                    <input name="txtMensaje" type="text" class="TextoFondo" id="txtMensaje" value="<?=$mensaje?>" readonly />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" id="btnVerificar" value="VERIFICAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
