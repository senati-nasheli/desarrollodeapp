<?php
if (isset($_POST["btnCalcular"])) {
    $matriz = array(
        array($_POST["txt11"], $_POST["txt12"]),
        array($_POST["txt21"], $_POST["txt22"]),
        array($_POST["txt31"], $_POST["txt32"])
    );

    $sumasFilas = array();
    foreach ($matriz as $fila) {
        $suma = array_sum($fila);
        $sumasFilas[] = $suma;
    }
}
?>

<html>
<head>
    <title>Problema 56</title>
    <link rel="stylesheet" type="text/css" href="estilos56.css">

</head>
<body>
    <form method="post" action="ejercicio56.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 56</strong></td>
            </tr>
            <tr>
                <td>ingrese los numeros</td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <input name="txt11" type="number" value="<?= $_POST["txt11"] ?? '' ?>" />
                            </td>
                            <td>
                                <input name="txt12" type="number" value="<?= $_POST["txt12"] ?? '' ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="txt21" type="number" value="<?= $_POST["txt21"] ?? '' ?>" />
                            </td>
                            <td>
                                <input name="txt22" type="number" value="<?= $_POST["txt22"] ?? '' ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="txt31" type="number" value="<?= $_POST["txt31"] ?? '' ?>" />
                            </td>
                            <td>
                                <input name="txt32" type="number" value="<?= $_POST["txt32"] ?? '' ?>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <?php foreach ($sumasFilas as $index => $suma) { ?>
                    <tr>
                        <td>Suma fila <?= $index + 1 ?></td>
                        <td><?= $suma ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </form>
</body>
</html>
