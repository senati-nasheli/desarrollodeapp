<?php
if (isset($_POST["btnCalcular"])) {
    $matriz = array(
        array($_POST["txt11"], $_POST["txt12"], $_POST["txt13"]),
        array($_POST["txt21"], $_POST["txt22"], $_POST["txt23"])
    );

    $k = $_POST["txtK"];
    $matrizMultiplicada = array();
    foreach ($matriz as $fila) {
        $filaMultiplicada = array();
        foreach ($fila as $numero) {
            $resultado = $numero * $k;
            $filaMultiplicada[] = $resultado;
        }
        $matrizMultiplicada[] = $filaMultiplicada;
    }

    $suma = 0;
    foreach ($matrizMultiplicada as $fila) {
        $suma += array_sum($fila);
    }
}
?>

<html>
<head>
    <title>Problema 58</title>
    <link rel="stylesheet" type="text/css" href="estilos58.css">
</head>
<body>
<form method="post" action="ejercicio58.php">
    <table>
        <tr>
            <td colspan="2"><strong>Problema 58</strong></td>
        </tr>
        <tr>
            <td>Ingrese los números</td>
            <td>
                <table>
                    <tr>
                        <td>
                            <input name="txt11" type="number" value="<?= $_POST["txt11"] ?? '' ?>" />
                        </td>
                        <td>
                            <input name="txt12" type="number" value="<?= $_POST["txt12"] ?? '' ?>" />
                        </td>
                        <td>
                            <input name="txt13" type="number" value="<?= $_POST["txt13"] ?? '' ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="txt21" type="number" value="<?= $_POST["txt21"] ?? '' ?>" />
                        </td>
                        <td>
                            <input name="txt22" type="number" value="<?= $_POST["txt22"] ?? '' ?>" />
                        </td>
                        <td>
                            <input name="txt23" type="number" value="<?= $_POST["txt23"] ?? '' ?>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Ingrese el valor de K</td>
            <td>
                <input name="txtK" type="number" value="<?= $_POST["txtK"] ?? '' ?>" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input name="btnCalcular" type="submit" value="Calcular" />
            </td>
        </tr>
        <?php if (isset($_POST["btnCalcular"])) { ?>
            <tr>
                <td>Matriz multiplicada por K</td>
                <td>
                    <table>
                        <?php foreach ($matrizMultiplicada as $fila) { ?>
                            <tr>
                                <?php foreach ($fila as $numero) { ?>
                                    <td><?= $numero ?></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Suma de los números de la matriz multiplicada</td>
                <td><?= $suma ?></td>
            </tr>
        <?php } ?>
    </table>
</form>
</body>
</html>
