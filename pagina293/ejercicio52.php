<?php
function encontrarMayorMenor($numeros) {
    $mayor = max($numeros);
    $menor = min($numeros);
    return array('mayor' => $mayor, 'menor' => $menor);
}

$numeros = array();

if (isset($_POST["btnCalcular"])) {
    $numero1 = $_POST["txtnumero1"];
    $numero2 = $_POST["txtnumero2"];
    $numero3 = $_POST["txtnumero3"];
    $numero4 = $_POST["txtnumero4"];

    $numeros = array($numero1, $numero2, $numero3, $numero4);

    $resultados = encontrarMayorMenor($numeros);
    $mayor = $resultados['mayor'];
    $menor = $resultados['menor'];
}
?>

<html>
<head>
    <title>Problema 52</title>
    <link rel="stylesheet" type="text/css" href="estilos.css">

</head>
<body>
    <form method="post" action="ejercicio52.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 52</strong></td>
            </tr>
            <tr>
                <td>Número 1</td>
                <td>
                    <input name="txtnumero1" type="number" id="txtnumero1" value="<?= $numeros[0] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Número 2</td>
                <td>
                    <input name="txtnumero2" type="number" id="txtnumero2" value="<?= $numeros[1] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Número 3</td>
                <td>
                    <input name="txtnumero3" type="number" id="txtnumero3" value="<?= $numeros[2] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Número 4</td>
                <td>
                    <input name="txtnumero4" type="number" id="txtnumero4" value="<?= $numeros[3] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Mayor</td>
                    <td><?= isset($mayor) ? $mayor : '' ?></td>
                </tr>
                <tr>
                    <td>Menor</td>
                    <td><?= isset($menor) ? $menor : '' ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
