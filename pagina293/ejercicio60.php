<?php
if (isset($_POST["btnObtenerMayores"])) {
    $matriz = array(
        array($_POST["txt11"], $_POST["txt12"], $_POST["txt13"]),
        array($_POST["txt21"], $_POST["txt22"], $_POST["txt23"]),
        array($_POST["txt31"], $_POST["txt32"], $_POST["txt33"]),
        array($_POST["txt41"], $_POST["txt42"], $_POST["txt43"])
    );

    $numerosMayores = array();
    for ($i = 0; $i < count($matriz[0]); $i++) {
        $mayor = $matriz[0][$i];
        for ($j = 1; $j < count($matriz); $j++) {
            if ($matriz[$j][$i] > $mayor) {
                $mayor = $matriz[$j][$i];
            }
        }
        $numerosMayores[] = $mayor;
    }
}
?>

<html>
<head>
    <title>Problema 60</title>
    <link rel="stylesheet" type="text/css" href="estilos60.css">
</head>
<body>
    <form method="post" action="ejercicio60.php">
        <table>
            <tr>
                <td colspan="3"><strong>Problema 60</strong></td>
            </tr>
            <tr>
                <td></td>
                <td>Columna 1</td>
                <td>Columna 2</td>
                <td>Columna 3</td>
            </tr>
            <tr>
                <td>Fila 1</td>
                <td>
                    <input name="txt11" type="number" value="<?= $matriz[0][0] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt12" type="number" value="<?= $matriz[0][1] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt13" type="number" value="<?= $matriz[0][2] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Fila 2</td>
                <td>
                    <input name="txt21" type="number" value="<?= $matriz[1][0] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt22" type="number" value="<?= $matriz[1][1] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt23" type="number" value="<?= $matriz[1][2] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Fila 3</td>
                <td>
                    <input name="txt31" type="number" value="<?= $matriz[2][0] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt32" type="number" value="<?= $matriz[2][1] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt33" type="number" value="<?= $matriz[2][2] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Fila 4</td>
                                <td>
                    <input name="txt41" type="number" value="<?= $matriz[3][0] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt42" type="number" value="<?= $matriz[3][1] ?? '' ?>" />
                </td>
                <td>
                    <input name="txt43" type="number" value="<?= $matriz[3][2] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnObtenerMayores" type="submit" value="Obtener Mayores" />
                </td>
            </tr>
            <?php if (isset($_POST["btnObtenerMayores"])) { ?>
                <?php foreach ($numerosMayores as $index => $mayor) { ?>
                    <tr>
                        <td>Número mayor columna <?= $index + 1 ?></td>
                        <td><?= $mayor ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </form>
</body>
</html>

