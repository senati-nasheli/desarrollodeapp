<?php
function ordenarAscendente($numeros) {
    sort($numeros);
    return $numeros;
}

function ordenarDescendente($numeros) {
    rsort($numeros);
    return $numeros;
}

$numeros = array();

if (isset($_POST["btnOrdenar"])) {
    $numero1 = $_POST["txtnumero1"];
    $numero2 = $_POST["txtnumero2"];
    $numero3 = $_POST["txtnumero3"];
    $numero4 = $_POST["txtnumero4"];
    $numero5 = $_POST["txtnumero5"];

    $numeros = array($numero1, $numero2, $numero3, $numero4, $numero5);

    $formaOrden = $_POST["selFormaOrden"];

    if ($formaOrden == "A") {
        $numerosOrdenados = ordenarAscendente($numeros);
    } else if ($formaOrden == "D") {
        $numerosOrdenados = ordenarDescendente($numeros);
    }
}
?>

<html>
<head>
    <title>Problema 54</title>
    <link rel="stylesheet" type="text/css" href="estilos54.css">
</head>
<body>
    <form method="post" action="ejercicio54.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 54</strong></td>
            </tr>
            <tr>
                <td>Número 1</td>
                <td>
                    <input name="txtnumero1" type="number" id="txtnumero1" value="<?=$numeros[0] ?? ''?>" />
                </td>
            </tr>
            <tr>
                <td>Número 2</td>
                <td>
                    <input name="txtnumero2" type="number" id="txtnumero2" value="<?=$numeros[1] ?? ''?>" />
                </td>
            </tr>
            <tr>
                <td>Número 3</td>
                <td>
                    <input name="txtnumero3" type="number" id="txtnumero3" value="<?=$numeros[2] ?? ''?>" />
                </td>
            </tr>
            <tr>
                <td>Número 4</td>
                <td>
                    <input name="txtnumero4" type="number" id="txtnumero4" value="<?=$numeros[3] ?? ''?>" />
                </td>
            </tr>
            <tr>
                <td>Número 5</td>
                <td>
                    <input name="txtnumero5" type="number" id="txtnumero5" value="<?=$numeros[4] ?? ''?>" />
                </td>
            </tr>
            <tr>
                <td>Forma de orden</td>
                <td>
                    <select name="selFormaOrden">
                    <option value="A">Ascendente</option>
                    <option value="D">Descendente</option>
                </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnOrdenar" type="submit" id="btnOrdenar" value="Ordenar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnOrdenar"])) { ?>
                <tr>
                    <td>Números ordenados</td>
                    <td>
                        <?php
                        if (isset($numerosOrdenados)) {
                            echo implode(", ", $numerosOrdenados);
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>

