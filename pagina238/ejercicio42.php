<?php
//Variables
$n = 0;
$sum = 0;
$divisibles = array();

if(isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn"];
    for ($i=1; $i<=$n; $i++) {
        if ($i % 3 == 0 && $i % 5 == 0) {
            $sum += $i;
            array_push($divisibles, $i);
        }
    }
}

if(!isset($_POST["btnCalcular"]) && empty($n)) {
    echo "<script>alert('Por favor ingrese un número.');</script>";
}
?>
<html>
<head>
    <title>Problema 42</title>
    <link rel="stylesheet" href="estilos42.css">
</head>
<body>
    <form method="post" action="ejercicio42.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Problema 42</strong></td>
            </tr>
            <tr>
                <td width="81">Ingrese un numero</td>
                <td width="150">
                    <input name="txtn" type="text" id="txtn" value="<?=$n?>" />
                </td>
            </tr>
            
            <?php if (!empty($divisibles)) { ?>
            <tr>
                <td>Divisibles por 3 y 5</td>
                <td>
                    <?= implode(", ", $divisibles) ?>
                </td>
            </tr>
            <tr>
                <td>Suma de los numeros</td>
                <td>
                    <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$sum?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
