<?php
if (isset($_POST["btnInvertir"])) {
    $nombre = $_POST["txtNombre"];
    $nombreInvertido = strrev($nombre);
}
?>

<html>
<head>
    <title>Problema 62</title>
    <link rel="stylesheet" type="text/css" href="estilos62.css">
</head>
<body>
    <form method="post" action="ejercicio62.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 62</strong></td>
            </tr>
            <tr>
                <td>Ingrese un nombre</td>
                <td>
                    <input name="txtNombre" type="text" value="<?= $_POST["txtNombre"] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnInvertir" type="submit" value="Invertir" />
                </td>
            </tr>
            <?php if (isset($_POST["btnInvertir"])) { ?>
                <tr>
                    <td>Nombre invertido</td>
                    <td><?= $nombreInvertido ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
