<?php
if (isset($_POST["btnEncriptar"])) {
    $fraseOriginal = $_POST["txtFraseOriginal"];
    $fraseEncriptada = "";

    for ($i = 0; $i < strlen($fraseOriginal); $i++) {
        $caracter = $fraseOriginal[$i];
        $ascii = ord($caracter);
        $asciiEncriptado = $ascii + 2;
        $caracterEncriptado = chr($asciiEncriptado);
        $fraseEncriptada .= $caracterEncriptado;
    }
}
?>

<html>
<head>
    <title>Problema 69</title>
    <link rel="stylesheet" href="estilos70.css">
</head>
<body>
    <form method="post" action="ejercicio69.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 69</strong></td>
            </tr>
            <tr>
                <td>Frase original</td>
                <td>
                    <textarea name="txtFraseOriginal" rows="4" cols="50"><?= $_POST["txtFraseOriginal"] ?? '' ?></textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnEncriptar" type="submit" value="Encriptar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnEncriptar"])) { ?>
                <tr>
                    <td>Frase encriptada</td>
                    <td><?= $fraseEncriptada ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
